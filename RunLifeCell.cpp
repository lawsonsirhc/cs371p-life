// -----------------
// RunLife.cpp
// -----------------

// --------
// Includes
// --------
#include <iostream> // cin, cout
#include <sstream>  // istringstream
#include <cassert>  // assert
#include "Life.hpp"

// ------------
// Debug Macros
// ------------
#define DEBUG_RUN 0

// -------
// pragmas
// -------

#ifdef __clang__
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

// ----
// main
// ----

int main () {
    using namespace std;
    string s;
    getline(cin, s);

    istringstream iss(s);
    int numTests;
    iss >> numTests;
    if(DEBUG_RUN){
        cout << "numTests: " << numTests << endl;
    }

    assert(1 <= numTests && numTests <= 100);

    getline(cin, s);

    for(int i = 0; i < numTests; ++i){
        string temp;
        getline(cin, temp);
        istringstream iss(temp);
        int row;
        int col;
        iss >> row;
        iss >> col;
        assert(1 <= row && row <= 200);
        assert(1 <= col && col <= 200);

        if(DEBUG_RUN){
            cout << "Row: " << row << " Col: " << col << endl;
        }

        Life<Cell> life(row, col);

        getline(cin, temp);
        istringstream iss_1(temp);
        int nLines;
        iss_1 >> nLines;
        assert(1 <= nLines && nLines <= 700);

        if(DEBUG_RUN){
            cout << "Num lines: " << nLines << endl;
        }

        for(int j = 0; j < nLines; ++j){
            string line;
            getline(cin, line);
            istringstream iss(line);

            int pos_r;
            int pos_c;
            iss >> pos_r;
            iss >> pos_c;

            if(DEBUG_RUN){
                cout << "Setting position: (" << pos_r << ", " << pos_c << ") alive" << endl;
            }

            life.setAlive(pos_r, pos_c);
        }
        
        getline(cin, temp);
        istringstream iss_2(temp);

        int sim;
        int output;
        iss_2 >> sim;
        iss_2 >> output;
        assert(1 <= sim && sim <= 2000);
        assert(1 <= output && output <= 200);

        cout << "*** Life<Cell> " << row << "x" << col << " ***" << endl << endl;
        life.printState();
        cout << endl;
        cout << endl;

        for(int sim_num = 1; sim_num <= sim; ++sim_num){
            life.nextGeneration();
            if(sim_num % output == 0) {
                life.printState();
                if((sim-sim_num) / output != 0) {
                    cout << endl;
                    cout << endl;
                }
            }
        }

        if(i != numTests - 1) {
            cout << endl;
            cout << endl;
            getline(cin, temp);
        }

        if(DEBUG_RUN) {
            cout << endl;
            cout << "FINISHED TEST: " << endl;
        }


    }
    return 0;
}