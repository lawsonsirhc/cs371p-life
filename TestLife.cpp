// -----------------
// TestLife.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// -------
// pragmas
// -------

#ifdef __clang__
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <string>    // string
#include <sstream>   // std::stringsteam

using namespace std;

#include <iostream>

#include "gtest/gtest.h"

#include "ConwayCell.hpp"
#include "FredkinCell.hpp"
#include "Life.hpp"
#include <vector>


// Evolve, born, kill, state, printCell, validNeighbors()
TEST(ConwayCellEvolveDead, test0) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test1) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(1);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test2) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(2);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test3) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(ConwayCellEvolveDead, test4) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test5) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(5);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test6) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(6);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test7) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(7);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveDead, test8) {
    ConwayCell* c = new ConwayCell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(8);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test0) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test1) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(1);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test2) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(2);
    ASSERT_EQ(result, 1);
}

TEST(ConwayCellEvolveAlive, test3) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(ConwayCellEvolveAlive, test4) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test5) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(5);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test6) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(6);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test7) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(7);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellEvolveAlive, test8) {
    ConwayCell* c = new ConwayCell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(8);
    ASSERT_EQ(result, 0);
}

TEST(ConwayCellNeighbors, test0) {
    ConwayCell* c = new ConwayCell();
    vector<pair<int, int>> ans = {{-1,-1}, {-1, 0}, {-1, 1}, { 0,-1}, { 0, 1}, { 1, -1}, { 1, 0}, { 1, 1}};
    vector<pair<int, int>> c_neighbors = c->validNeighbors();

    ASSERT_EQ(ans.size(), c_neighbors.size());
    for(int i = 0; i < (int)c_neighbors.size(); ++i) {
        ASSERT_EQ(ans[i], c_neighbors[i]);
    }
}

TEST(ConwayCellPrint, test0) {
    string expected = ".";
    ConwayCell* c = new ConwayCell();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

TEST(ConwayCellPrint, test1) {
    string expected = "*";
    ConwayCell* c = new ConwayCell();
    c->born();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

// Evolve, Born, Kill, state, printCell, validNeighbors()
TEST(FredkinCellEvolveDead, test0) {
    FredkinCell* f = new FredkinCell();
    ASSERT_FALSE(f->state());
    int result = f->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveDead, test1) {
    FredkinCell* f = new FredkinCell();
    ASSERT_FALSE(f->state());
    int result = f->evolve(1);
    ASSERT_EQ(result, 1);
}

TEST(FredkinCellEvolveDead, test2) {
    FredkinCell* f = new FredkinCell();
    ASSERT_FALSE(f->state());
    int result = f->evolve(2);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveDead, test3) {
    FredkinCell* f = new FredkinCell();
    ASSERT_FALSE(f->state());
    int result = f->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(FredkinCellEvolveDead, test4) {
    FredkinCell* f = new FredkinCell();
    ASSERT_FALSE(f->state());
    int result = f->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveAlive, test0) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveAlive, test1) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(1);
    ASSERT_EQ(result, 1);
}

TEST(FredkinCellEvolveAlive, test2) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(2);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveAlive, test3) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(FredkinCellEvolveAlive, test4) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(FredkinCellEvolveAge, test0) {
    FredkinCell* f = new FredkinCell();
    f->born();
    ASSERT_TRUE(f->state());
    int result = f->evolve(1);
    ASSERT_EQ(result, 1);

    result = f->evolve(1);
    ASSERT_EQ(result, 2);

    result = f->evolve(1);
    ASSERT_EQ(result, 3);
}

TEST(FredkinCellNeighbors, test0) {
    FredkinCell* f = new FredkinCell();
    vector<pair<int, int>> ans = {{-1, 0}, { 0,-1}, { 0, 1}, { 1, 0}};
    vector<pair<int, int>> f_neighbors = f->validNeighbors();

    ASSERT_EQ(ans.size(), f_neighbors.size());
    for(int i = 0; i < (int)f_neighbors.size(); ++i) {
        ASSERT_EQ(ans[i], f_neighbors[i]);
    }
}

TEST(FredkinCellPrint, test0) {
    string expected = "-";
    FredkinCell* f = new FredkinCell();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    f->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

TEST(FredkinCellPrint, test1) {
    string expected = "0";
    FredkinCell* f = new FredkinCell();
    f->born();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    f->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

TEST(FredkinCellPrint, test2) {
    string expected = "2";
    FredkinCell* f = new FredkinCell();
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    f->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

TEST(FredkinCellPrint, test3) {
    string expected = "+";
    FredkinCell* f = new FredkinCell();
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);
    f->evolve(1);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    f->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);

}

// printCell, evolve, born, state, printCell, validNeighbors
TEST(CellEvolveDead, test0) {
    Cell* c = new Cell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(CellEvolveDead, test1) {
    Cell* c = new Cell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(1);
    ASSERT_EQ(result, 1);
}

TEST(CellEvolveDead, test2) {
    Cell* c = new Cell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(2);
    ASSERT_EQ(result, 0);
}

TEST(CellEvolveDead, test3) {
    Cell* c = new Cell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(CellEvolveDead, test4) {
    Cell* c = new Cell();
    ASSERT_FALSE(c->state());
    int result = c->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(CellEvolveAlive, test0) {
    Cell* c = new Cell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(0);
    ASSERT_EQ(result, 0);
}

TEST(CellEvolveAlive, test1) {
    Cell* c = new Cell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(1);
    ASSERT_EQ(result, 1);
}

TEST(CellEvolveAlive, test2) {
    Cell* c = new Cell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(2);
    ASSERT_EQ(result, 0);
}

TEST(CellEvolveAlive, test3) {
    Cell* c = new Cell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(3);
    ASSERT_EQ(result, 1);
}

TEST(CellEvolveAlive, test4) {
    Cell* c = new Cell();
    c->born();
    ASSERT_TRUE(c->state());
    int result = c->evolve(4);
    ASSERT_EQ(result, 0);
}

TEST(CellFredkinNeighbors, test0) {
    Cell* c = new Cell();
    vector<pair<int, int>> ans = {{-1, 0}, { 0,-1}, { 0, 1}, { 1, 0}};
    vector<pair<int, int>> c_neighbors = c->validNeighbors();

    ASSERT_EQ(ans.size(), c_neighbors.size());
    for(int i = 0; i < (int)c_neighbors.size(); ++i) {
        ASSERT_EQ(ans[i], c_neighbors[i]);
    }
}

TEST(CellConwayNeighbors, test0) {
    Cell* c = new Cell();
    c->evolve(1);
    c->evolve(1);
    c->evolve(1);
    c->evolve(1);
    vector<pair<int, int>> ans = {{-1,-1}, {-1, 0}, {-1, 1}, { 0,-1}, { 0, 1}, { 1, -1}, { 1, 0}, { 1, 1}};
    vector<pair<int, int>> c_neighbors = c->validNeighbors();

    ASSERT_EQ(ans.size(), c_neighbors.size());
    for(int i = 0; i < (int)c_neighbors.size(); ++i) {
        ASSERT_EQ(ans[i], c_neighbors[i]);
    }
}

TEST(CellPrint, test0) {
    string expected = "-";
    Cell* c = new Cell();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}

TEST(CellPrint, test1) {
    string expected = "0";
    Cell* c = new Cell();
    c->born();

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}

TEST(CellPrint, test2) {
    string expected = "1";
    Cell* c = new Cell();
    c->born();
    c->evolve(1);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}

TEST(CellPrint, test3) {
    string expected = "2";
    Cell* c = new Cell();
    c->born();
    c->evolve(1);
    c->evolve(1);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}

TEST(CellPrint, test4) {
    string expected = "*";
    Cell* c = new Cell();
    c->born();
    c->evolve(1);
    c->evolve(1);
    c->evolve(1);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}

TEST(CellPrint, test5) {
    string expected = ".";
    Cell* c = new Cell();
    c->born();
    c->evolve(1);
    c->evolve(1);
    c->evolve(1);
    c->evolve(4);

    stringstream buffer;

    streambuf* prevcoutbuf = cout.rdbuf(buffer.rdbuf());

    cout << endl;
    c->printCell();

    string response = buffer.str();
    int result = response.compare(expected);

    cout.rdbuf(prevcoutbuf);

    ASSERT_TRUE(result);
}