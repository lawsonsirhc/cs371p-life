// ----------------
// AbstractCell.cpp
// ----------------

// --------
// Includes
// --------
#include "AbstractCell.hpp"
#include "ConwayCell.hpp"
#include "FredkinCell.hpp"

#define DEBUG_C 0

using namespace std;

// Setter for _state
void AbstractCell::born() {
    _state = true;
}

// Setter for _state
void AbstractCell::kill() {
    _state = false;
}

// Getter for _state
bool AbstractCell::state() {
    return _state;
}

// Will just save a new instance of fredkinCell
Cell::Cell() {
    _p = new FredkinCell();
    if(DEBUG_C) {
        cout << "IN CELL CONSTRUCTOR" << endl;
        cout << "new _p is: ";
        cout << _p << endl << endl;
    }
}

// Need to make sure that on deletion, the internal pointer is deleted.
Cell::~Cell() {
    if(DEBUG_C) {
        cout << "IN CELL DESTRUCTOR" << endl;
        cout << "deleteing _p: ";
        cout << _p << endl << endl;
    }
    // Why does commenting this work?
    //delete _p;
}

// Will simply call _p's printCell
void Cell::printCell() {
    if(DEBUG_C) {
        cout << "IN CELL PRINTCELL" << endl;
        cout << "_p is: ";
        cout << _p << endl;
    }
    _p->printCell();
}

// Will call _p's evolve
// Does need to make sure that if the age returned is 2,
// it needs to be replaced with a conwayCell (make sure that it is alive)
int Cell::evolve(int neighbors) {
    assert(neighbors >= 0 && neighbors < 9);
    int code = _p->evolve(neighbors);
    if(code == 2) {
        delete _p;
        _p = new ConwayCell();
        _p->born();
    }
    assert(code >= 0);
    return code;
}

// Setter for Cell
void Cell::born() {
    _p->born();
}

// Getter for Cell
bool Cell::state() {
    return _p->state();
}

// Returning _p's valid neighbors.
vector<pair<int,int>> Cell::validNeighbors() {
    return _p->validNeighbors();
}