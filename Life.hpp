#ifndef LIFE_H // include Guard
#define LIFE_H

// --------
// Life.hpp
// --------

// --------
// Includes
// --------
#include <vector>
#include <iostream>
#include "AbstractCell.hpp"
using namespace std;

// ------------
// Debug Macros
// ------------
#define DEBUG_NEIGHBORS 0
#define DEBUG_CONSTRUCTOR 0

// Constant Value for the Moat
#define MOAT 1

template<class T>
class Life {
    // Update this to make the print status much easier on yourself
    // friend ostream& << (ostream& given, const life& me){

    // }

private:
    // This will contain the grid of currently alive Cells of type T.
    vector<vector<T>> _grid;

    // Keeps track of which generation we are currently on.
    int _current_generation;

    // Keeps track of how many Cells are currently alive at this point in time.
    int _current_alive;

public:
    // Life will be initialized with 2 ints that represent the dimensions of the internal grid
    // This will also defaultly initialize current generation and current alive to 0.
    Life(int row_len, int col_len) : _grid(), _current_generation(0), _current_alive(0) {
        // As by this point in time we want to have grid initialized, we want to reserve the space for
        // the grid without initializing anything
        if(DEBUG_CONSTRUCTOR) cout << "ENTERED INITIALIZATION" << endl;
        // The grid will internally hold a "moat" that separates the actual grid from the boarder.
        // Helps with checking if neighbors are in bounds or not.
        _grid.reserve(row_len+MOAT*2);

        // Next we go through the entire dimensions of the grid to initialize everything accordingly.
        if(DEBUG_CONSTRUCTOR) cout << "Row_len: " << row_len << ", Col_len: " << col_len << endl;
        for(int i = 0; i < row_len + MOAT*2; ++i) {
            if(DEBUG_CONSTRUCTOR) cout << "STARTED A ROW: " << i << endl;
            vector<T> temp;
            temp.reserve(col_len + MOAT*2);
            for(int j = 0 ; j < col_len + MOAT*2; ++j) {
                if(DEBUG_CONSTRUCTOR) cout << "STARTED A COLUMN: " << j << endl;
                //T* item = new T();
                temp.push_back(T());
                if(DEBUG_CONSTRUCTOR) cout << "ENDED A COLUMN " << j << endl;
            }
            _grid.push_back(temp);
            if(DEBUG_CONSTRUCTOR) cout << "ENDED A ROW: " << i << endl << endl;
        }

        if(DEBUG_CONSTRUCTOR) cout << "FINISH INITIALIZATION" << endl;
    }

    // Nothing special with the destructor.
    ~Life() = default;

    // This method will only be called upon in the initial stages of the program.
    // Given a row and column cordinate, it will make that Cell become alive. By calling the "born()" function.

    // Makes sure to handle duplicates.
    void setAlive(int r, int c) {
        assert(r < (int)_grid.size() && r >= 0);
        assert(c < (int)_grid[0].size() && c >= 0);
        if(!_grid[r+MOAT][c+MOAT].state()) {
            _grid[r+MOAT][c+MOAT].born();
            ++_current_alive;
        }
    }

    // This method will progress the state forward.
    // It will first construct an array that will model the previous state of the grid
    // only taking note of the nodes that were alive.
    void nextGeneration() {
        // Create that neighbor_array with initial value of 0.
        vector<vector<int>> prev_state(_grid.size(), vector<int>(_grid[0].size(), 0));

        for(int i = MOAT; i < (int)_grid.size() - MOAT; ++i) {
            for(int j = MOAT; j < (int)_grid[i].size() - MOAT; ++j) {
                // Because state returns a bool, it's fine to cast as int and add it.
                prev_state[i][j] += (int)_grid[i][j].state();
            }
        }

        // Next based off of the previous state we will find out how many neighbors a particular
        // cell has and evolve it based on its number of neighbors.
        int cur_alive = 0;
        for(int i = MOAT; i < (int)prev_state.size() - MOAT; ++i) {
            for(int j = MOAT; j < (int)prev_state[i].size() - MOAT; ++j) {
                // Get the neighbors of this cell
                vector<pair<int,int>> cur_neighbors = _grid[i][j].validNeighbors();

                // next find out the number of neighbors it has
                int num_neighbors = 0;
                for(int cur_n = 0; cur_n < (int)cur_neighbors.size(); ++cur_n) {
                    pair<int, int> temp = cur_neighbors[cur_n];
                    int temp_r = i + temp.first;
                    int temp_c = j + temp.second;
                    if(prev_state[temp_r][temp_c]) {
                        ++num_neighbors;
                    }
                }

                // Then call evolve on the cell with the aquired number of neighbors.
                _grid[i][j].evolve(num_neighbors);
                if(_grid[i][j].state()) {
                    ++cur_alive;
                }
            }
        }

        // Lastly make sure to update the current generation and the current alive.
        ++_current_generation;
        _current_alive = cur_alive;
    }

    // Will print the current state of the board, relies on each cells individual print method to print
    // correctly.

    // Only thing to be careful of is that we do not print cells that are in the moat.
    void printState() {
        cout << "Generation = " << _current_generation << ", Population = " << _current_alive << "." << endl;
        for(int i = MOAT; i < (int)_grid.size() - MOAT; ++i) {
            for(int j = MOAT; j < (int)_grid[i].size() - MOAT; ++j) {
                _grid[i][j].printCell();
            }
            if(i != (int)_grid.size()-2) {
                cout << endl;
            }
        }
    }

};

#endif