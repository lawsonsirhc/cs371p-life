#ifndef ABSTRACTCELL_H
#define ABSTRACTCELL_H


// ----------------
// AbstractCell.hpp
// ----------------

// --------
// Includes
// --------
#include <vector>
#include <algorithm>
#include <iostream>
#include <cassert>
using namespace std;

class AbstractCell {
private:
    // To tell me whether the current cell is dead or not
    bool _state;

public:
    // In case I want to initialize a cell as alive or dead.
    AbstractCell(bool s) : _state(s) {}

    virtual ~AbstractCell() {

    }

    // Will be a stub for the children to implement
    // will evolve the cell in accordance to it's requirements
    int virtual evolve(int neighbors) = 0;

    // A setter for _state, sets state to true.
    void born();

    // A setter for _state, sets state to false.
    void kill();

    // Will be a stub for the children to implement.
    // Will print the cell in accordance to it's requirements
    virtual void printCell() = 0;

    // Getter for _state.
    bool state();

    // Will be a stub for the children to implement.
    // Will return all valid neighbors of this current cell
    virtual vector<pair<int,int>> validNeighbors() = 0;
};

class Cell {
private:
    // Will hold the cell this cell is currently maintaining.
    AbstractCell* _p;

public:
    // Should initialize _p to a new fredkincell
    Cell();

    // Should delete the currently help _p as it is on the heap.
    ~Cell();

    // Making sure that copy constructor is defaulted
    Cell (const Cell&) = default;

    // Making sure that copy assignment is defaulted
    Cell& operator= (const Cell&) = default;

    // Will print the cell that is currently being held
    void printCell();

    // Will evolve the cell that is currently being held
    int evolve(int neighbors);

    // Will set the current cell alive.
    void born();

    // Will return the current state of the cell
    bool state();

    // Will return the valid neighbors of this cell.
    vector<pair<int,int>> validNeighbors();

};

#endif