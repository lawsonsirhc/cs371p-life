#ifndef FREDKINCELL_H
#define FREDKINCELL_H

// ---------------
// FredkinCell.hpp
// ---------------

// --------
// Includes
// --------
#include "AbstractCell.hpp"
#include <iostream>
#include <cassert>

// FredkinCell does not need to create any methods that are not
// already in AbstractCell

class FredkinCell : public AbstractCell {
private:
    // Due to the Specs, Fredkin does need an additional age member
    int _age;
    // This is to communicate with Life as to what neighbors this type of Cell has.
    static vector<pair<int,int>> _neighbors;

public:
    // All cells are going to be initialized as dead cells, age is also set to 0
    FredkinCell() : AbstractCell(false), _age(0) {}

    // No heap stored or special classes for member variables so we can
    // use the default destructor.
    ~FredkinCell() = default;

    // This method will determine whether the cell will live or die based
    // on the number of neighbors it has (this is passed in).
    // For fredkin it will return the age if it is alive, or 0 if it is dead
    // or 1 if it is newly alive
    int evolve(int neighbors);

    // will print the cell according to the specs.
    void printCell();

    // Will return the _neighbors array to Life.
    vector<pair<int,int>> validNeighbors();
};

#endif