#ifndef CONWAYCELL_H // Include guard
#define CONWAYCELL_H

// --------------
// ConwayCell.hpp
// --------------
#include "AbstractCell.hpp"
#include <iostream>
#include <cassert>

// ConwayCell does not need to create any methods that are not provided
// in Abstract Cell

// Thus no new functions.
class ConwayCell : public AbstractCell {
private:
    // This is to communicate with Life as to what neighbors this type of Cell has.
    static vector<pair<int,int>> _neighbors;

public:
    // All cells are going to be initialized as dead cells.
    ConwayCell() : AbstractCell(false) {}

    // No heap stored or special classes for member variables so we can
    // use the default destructor.
    ~ConwayCell() = default;

    // This method will determine whether the cell will live or die based
    // on the number of neighbors it has (this is passed in).
    // For conway it will only return 0 or 1 depending on whether it is alive
    // or dead.
    int evolve(int neighbors);

    // will print the cell according to the specs.
    void printCell();

    // Will return the _neighbors array to Life.
    vector<pair<int,int>> validNeighbors();
};

#endif