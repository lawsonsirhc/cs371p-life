// --------------
// ConwayCell.cpp
// --------------

// --------
// Includes
// --------
#include "ConwayCell.hpp"

// The valid neighbors for conway are all directions, thus we have the direction pairs of everything except (0, 0), or self.
vector<pair<int,int>> ConwayCell::_neighbors = {{-1,-1}, {-1, 0}, {-1, 1}, { 0,-1}, { 0, 1}, { 1, -1}, { 1, 0}, { 1, 1}};

int ConwayCell::evolve(int neighbors) {
    assert(neighbors >= 0 && neighbors <= 8);
    // In accordance to Conway's requirements:

    // If the cell is not alive and it has 3 neighbors, it becomes alive.
    // If the cell is alive and it has either less than 2 neighbors or more than 3 neighbors
    // it dies of overcrowding.
    if(!state() && neighbors == 3) {
        born();
    } else if(state() && (neighbors < 2 || neighbors > 3)) {
        kill();
    }

    assert(state() || !state());
    // Next because we only return 0 or 1, we just cast the state as an int.
    return (int)state();
}

// Here we just print the state of the cell, * means alive, . means dead.
void ConwayCell::printCell() {
    using namespace std;
    if(state()) {
        cout << "*";
    } else {
        cout << ".";
    }
}

// Getter for the valid neighbors of Conway.
vector<pair<int,int>> ConwayCell::validNeighbors() {
    return _neighbors;
}