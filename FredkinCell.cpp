// ---------------
// FredkinCell.cpp
// ---------------

// --------
// Includes
// --------
#include "FredkinCell.hpp"

// For fredkin, it's valid neighbors are only it's cardinal neighbors, thus we have the following
// direction pairs.
vector<pair<int,int>> FredkinCell::_neighbors = {{-1, 0}, { 0,-1}, { 0, 1}, { 1, 0}};

int FredkinCell::evolve(int neighbors) {
    assert(neighbors >= 0 && neighbors <= 4);
    // In accordance to the requirements, we first need to keep track of what the previous state
    // was before calling evolve.
    bool was_alive = state();

    // Next, if the cell is alive and it has either 0, 2, or 4 neighbors, then it dies of overcrowding.
    // if the cell is not alive, and it has either 1 or 3 neighbors, then it comes back alive.
    if(state() && neighbors != 3 && neighbors != 1) {
        kill();
        return 0;
    } else if(!state() && (neighbors == 3 || neighbors == 1)) {
        born();
        return 1;
    }

    // If nothing happens, then we want to check whether it was alive or not.
    // if it was, increase the age by 1.
    if(was_alive && was_alive == state()) {
        ++_age;
    }

    assert(_age >= 0);
    // return age to help cell make the correct conversion.
    return _age;
}

// Here we just need to follow the following:
// if it's not alive, print -
// if it's alive and it's age is less than 10, print age
// else print +
void FredkinCell::printCell() {
    using namespace std;
    if(!state()) {
        cout << "-";
    } else if(_age >= 10) {
        cout << "+";
    } else {
        cout << _age;
    }
}

// Getter for valid neighbors for fredkin.
vector<pair<int,int>> FredkinCell::validNeighbors() {
    return _neighbors;
}